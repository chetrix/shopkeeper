import React from 'react';
import { Container } from 'react-bootstrap';
import Header from './components/Header/header';
import Footer from './components/Footer/footer';
import Home from './components/Home/home';
import Pdp from './components/PDP/pdp';
import Cart from './components/Cart/cart';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './components/Login/Login';
import Register from './components/Registration/register';
import Profile from './components/Profile/profile';
import Address from './components/Address/address';
import Payment from './components/Payment/payment';
import PlaceOrder from './components/PlaceOrder/placeOrder';
import PaymentPortal from './components/PaymentPortal/PaymentPortal';
import Order from './components/Order/order';
import users from './components/Users/users';
import userEdit from './components/UserEdit/userEdit';
import ProductList from './components/ProductListForAdmin/ProductList';
import ProductEdit from './components/ProductEditByAdmin/ProductEdit';
import OrderList from './components/OrdersListByAdmin/OrdersList';

function App() {
  return (
    <Router>
      <Header />
      <main className='py-3'>
        <Container>
          <Route path='/' component={Home} exact />
          <Route path='/page/:pageNumber' component={Home} exact />
          <Route path='/search/:keyword' component={Home} exact />
          <Route path='/search/:keyword/page/:pageNumber' component={Home} exact />
          <Route path='/order/:id' component={Order}/>
          <Route path='/product/:id' component={Pdp}/>
          <Route path='/cart/:id?' component={Cart}/>
          <Route path='/login' component={Login}/>
          <Route path='/register' component={Register}/>
          <Route path='/profile' component={Profile}/>
          <Route path='/shipping' component={Address}/>
          <Route path='/payment' component={Payment}/>
          <Route path='/placeOrder' component={PlaceOrder}/>
          <Route path='/paymentPortal' component={PaymentPortal}/>
          <Route path='/admin/users' component={users}/>
          <Route path='/admin/user/:id/edit' component={userEdit}/>
          <Route path='/admin/products' component={ProductList} exact/>
          <Route path='/admin/products/:pageNumber' component={ProductList} exact/>
          <Route path='/admin/product/:id/edit' component={ProductEdit}/>
          <Route path='/admin/orders' component={OrderList}/>
        </Container>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
