import React from 'react';
import { Helmet } from 'react-helmet';

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta
        name='description'
        content={description}
      />
      <meta name='keyword' keywords={keywords}/>
    </Helmet>
  );
};

Meta.defaultProps = {
    title:'Welcome To Sano Ghumti',
    description: 'We sell you',
    keywords: 'Electronics and Groceries'
}

export default Meta;
